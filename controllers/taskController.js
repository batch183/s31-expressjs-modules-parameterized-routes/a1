// It will allow us to use the contents of the task.js file in the model folder.
const Task = require("../models/task");

// Get all tasks
module.exports.getAllTasks = () =>{
	//The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/postman.
	return Task.find({}).then(result => result);
}

// Create a task
module.exports.createTask = (reqBody) =>{

	// Create a task object based on the mongoose model "Task"
	let newTask = new Task({
		// Sets the "name" property with the value received from the postman.
		name: reqBody.name
	})
		// Using callback function: newUser.save((saveErr, savedTask) => {})
		// Using .then method: newTask.save().then((task, error) =>{})
	return newTask.save().then((task, error) =>{
		if(error){
			console.log(error)
			return false
		}
		else{
			// Returns the new task object saved in the database back to the postman
			return task
		}
	})
}


// Delete a task
/*

	1. look for the corresponding id provided in URL/route
	2. Delete the task uding "findbyIdAndRemove"

*/

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}
	})
}

//Update a task

module.exports.updateTask = (taskId,reqBody) => {
	//findById find({"_id":"value"})
	return Task.findById(taskId).then((result,error) => {
		if(error) {
			console.log(error);
			return false;
		}
		else {
			result.name = reqBody.name;

			return result.save().then((updatedTaskName, updateErr) => {
				if(updateErr){
					console.log(updateErr);
					return false;
				}
				else{
					return updatedTaskName;

				}
			})
		}
	})
}

// ACTIVITY

module.exports.getTask = (taskId,reqBody) =>{
	//The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/postman.
	return Task.findById(taskId).then(result => result);
}

module.exports.completeTask = (taskId,reqBody) => {
	//findById find({"_id":"value"})
	return Task.findById(taskId).then((result,error) => {
		if(error) {
			console.log(error);
			return false;
		}
		else {
			
			result.status = "complete";
			

			return result.save().then((completedTaskName, completeErr) => {
				if(completeErr){
					console.log(completeErr);
					return false;
				}
				else{
					return completedTaskName;

				}
			})
		}
	})
}